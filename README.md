# Singularity R: rstudio

Singularity image for [RStudio Desktop](https://www.rstudio.com/products/RStudio/) based on the [rocker/r-ver](https://hub.docker.com/r/rocker/r-ver/) docker image.

## Build

You can build a local Singularity image named `singularity-rstudio.img` with:

```
$ sudo singularity build singularity-rstudio.img Singularity
```

## Deploy

Instead of building it yourself you can download the pre-built image from
[Duke Singularity Registry](https://research-singularity-registry.oit.duke.edu) with:

```
wget https://research-singularity-registry.oit.duke.edu/mjs136/singularity-rstudio.img
chmod +x singularity-rstudio.img
```

## Usage

Help

```console
$ singularity help singularity-rstudio.img


  RStudio Desktop version 1.1.442
  R version 3.4.4 (2018-03-15) -- "Someone to Lean On"

  Usage:
  $ singularity run singularity-rstudio.img [args]
  $ singularity run --app R singularity-rstudio.img [args]
  $ singularity run --app Rscript singularity-rstudio.img [args]
  $ singularity run --app rstudio singularity-rstudio.img
```

## Run



No particular command is launched using the default run command, rather it is left to the user to specify:

```bash
singularity run singularity-rstudio.img [args]
```

Where `[args]` is generally one of {`rstudio`, `R [args]`, `Rscript [args]`}

### R

The `R` command is launched as an explicit app:

```bash
singularity run --app R singularity-rstudio.img [args]
```

Example:

```console
$ singularity run --app R singularity-rstudio.img --version
R version 3.4.4 (2018-03-15) -- "Someone to Lean On"
Copyright (C) 2018 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under the terms of the
GNU General Public License versions 2 or 3.
For more information about these matters see
http://www.gnu.org/licenses/.
```

### Rscript

The `Rscript` command is launched as an explicit app:

```bash
singularity run --app Rscript singularity-rstudio.img [args]
```

Example:

```console
$ singularity run --app Rscript singularity-rstudio.img --version
R scripting front-end version 3.4.4 (2018-03-15)
```

### RStudio

**NOTE**: X-11 forwarding required if being used from terminal only

The `rstudio` command is launched as an explicit app:

```bash
singularity run --app rstudio singularity-rstudio.img
```

Example:

```console
$ singularity run --app rstudio singularity-rstudio.img
libGL error: No matching fbConfigs or visuals found
libGL error: failed to load driver: swrast
```

**RStudio Desktop UI**: (using X11)

<img width="80%" alt="RStudio Desktop" src="https://user-images.githubusercontent.com/5332509/37848039-221fdf5e-2ea9-11e8-8f9c-db199ad4f6d2.png">


**NOTE**:

- The `LibGL error:` errors were observed while testing using a remote CentOS 7 VM to run the Singularity image and the UI was rendered using X11. No other errors were denoted during testing.
- On exit, the terminal displayed:

    ```console
    QApplication::qAppName: Please instantiate the QApplication object first
    ```
